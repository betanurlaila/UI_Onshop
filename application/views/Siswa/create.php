<?php section('content') ?>
 <div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<!-- Main content -->
		<section class="content">
		  <div class="row">
			<div class="col-xs-12">
			  <div>		
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Tambah Golongan</h3>
					</div>
					<div class="box-body">
		 				<?= $this->form->open('siswa/save', 'class="form-horizontal"') ?>
            			<?php getview('siswa/form') ?>  
            			<?= $this->form->close() ?> 
					</div>
				</div>
			  </div>
			</div><!-- /.col -->
		  </div><!-- /.row -->
		</section><!-- /.content -->
	  </div><!-- /.content-wrapper -->


<?php endsection() ?>

<?php getview('layouts/layout') ?>