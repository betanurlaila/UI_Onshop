<div class="col-sm-9 padding-right">
          <div class="features_items">
            <h2 class="title text-center">Sale & New Items</h2>
            <div class="col-sm-4">
              <div class="product-image-wrapper">
                <div class="single-products">
                    <div class="productinfo text-center">
                      <img src="public/img/depan/ipod_classic_1-228x228.jpg" height="250" alt="" />
                      <h3><strike>$370.00</strike></h3>
                      <p>iPod Classic</p>
                      <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                    </div>
                    <div class="product-overlay">
                      <div class="overlay-content">
                      	<h3><strike>$370.00</strike></h3>
                        <h2>$365.00</h2>
                        <p>iPod Classic</p>
                        <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                      </div>
                    </div>
                    <img src="public/img/home/sale.png" class="new" alt="" />
                </div>
                <div class="choose">
                  <ul class="nav nav-pills nav-justified">
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="product-image-wrapper">
                <div class="single-products">
                  <div class="productinfo text-center">
                    <img src="public/img/depan/samsung-galaxy-s7-edge-2.jpg" height="250" alt="" />
                    <h3><strike>$700.00</strike></h3>
                    <p>Samsung Galaxy S7</p>
                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                  </div>
                  <div class="product-overlay">
                    <div class="overlay-content">
                      <h3><strike>$700.00</strike></h3>
                      <h2>$698.00</h2>
                      <p>Samsung Galaxy S7</p>
                      <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                    </div>
                  </div>
                  <img src="public/img/home/sale.png" class="new" alt="" />
                </div>
                <div class="choose">
                  <ul class="nav nav-pills nav-justified">
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="product-image-wrapper">
                <div class="single-products">
                  <div class="productinfo text-center">
                    <img src="public/img/depan/l_10138635_004.jpg" height="250"alt="" />
                    <h3>$2,000.00</h3>
                    <p>iPhone 6s Rose Gold</p>
                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                  </div>
                  <div class="product-overlay">
                    <div class="overlay-content">
                    <h3>$2,000.00</h3>
                    <p>iPhone 6s Rose Gold</p>>
                      <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                    </div>
                  </div>
                  <img src="public/img/home/new.png" class="new" alt="" />
                </div>
                <div class="choose">
                  <ul class="nav nav-pills nav-justified">
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="product-image-wrapper">
                <div class="single-products">
                  <div class="productinfo text-center">
                    <img src="public/img/depan/iphone_1-200x200.jpg" height="250" alt="" />
                    <h3><strike>$550.00</h3></strike>
                    <p>iPhone 4</p>
                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                  </div>
                  <div class="product-overlay">
                    <div class="overlay-content">
                      <h3><strike>$550.00</h3></strike>
                      <h2>$540.00</h2>
                      <p>iPhone 4</p>
                      <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                    </div>
                  </div>
                  <img src="public/img/home/sale.png" class="new" alt="" />
                </div>
                <div class="choose">
                  <ul class="nav nav-pills nav-justified">
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="product-image-wrapper">
                <div class="single-products">
                  <div class="productinfo text-center">
                    <img src="public/img/depan/img.jpg" height="250" alt="" />
                    <h3><strike>$315.00</strike></h3>
                    <p>Samsung Galaxy E7 4G Duos</p>
                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                  </div>
                  <div class="product-overlay">
                    <div class="overlay-content">
                    <h3><strike>$315.00</strike></h3>
                    <h2>$305.00</h2>
                    <p>Samsung Galaxy E7 4G Duos</p>
                      <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                    </div>
                  </div>
                  <img src="public/img/home/sale.png" class="new" alt="" />
                </div>
                <div class="choose">
                  <ul class="nav nav-pills nav-justified">
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="product-image-wrapper">
                <div class="single-products">
                  <div class="productinfo text-center">
                    <img src="public/img/depan/114201.jpeg" height="250" alt="" />
                    <h2>$350.00</h2>
                    <p>Azuz Zenfone Selfie</p>
                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                  </div>
                  <div class="product-overlay">
                    <div class="overlay-content">
                    <h2>$350.00</h2>
                    <p>Azuz Zenfone Selfie</p>
                      <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                    </div>
                  </div>
                  <img src="public/img/home/new.png" class="new" alt="" />
                </div>
                <div class="choose">
                  <ul class="nav nav-pills nav-justified">
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
<div class="category-tab"><!--category-tab-->
						<div class="col-sm-16">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#desktops" data-toggle="tab">Desktops</a></li>
								<li><a href="#laptops" data-toggle="tab">Laptops & Notebooks</a></li>
								<li><a href="#components" data-toggle="tab">Components</a></li>
								<li><a href="#tablets" data-toggle="tab">Tablets</a></li>
								<li><a href="#software" data-toggle="tab">Software</a></li>
								<li><a href="#phones" data-toggle="tab">Phones & PDAs</a></li>
								<li><a href="#cameras" data-toggle="tab">Cameras</a></li>
								<li><a href="#mp3players" data-toggle="tab">MP3 Players</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="desktops" >
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/apple_cinema_30-200x200.jpg" width="300" height="200" alt="" />
												<h2>$120.00</h2>
												<p>Apple Cinema 30"</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/samsung_syncmaster_941bw-228x228.jpg" width="300" height="200"alt="" />
												<h2>$110.00</h2>
												<p>Samsung SyncMaster 941BW</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/dsnew-desktop-drawer-3-3.jpg" width="300" height="200" alt="" />
												<h2>$1,105.00 </h2>
												<p>HP Pavilion All-in-One PC</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="laptops" >
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/hp_1-228x228.jpg" width="300" height="200" alt="" />
												<h2>$122.00</h2>
												<p>HP LP3065</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/macbook_air_1-228x228.jpg" width="300" height="200" alt="" />
												<h2>$1,202.00 </h2>
												<p>MacBook Air</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/macbook_1-228x228.jpg" width="300" height="200" alt="" />
												<h2>$602.00 </h2>
												<p>MacBook</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="components" >
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/main-hero.jpg" width="300" height="200" alt="" />
												<h2>$515.00</h2>
												<p>Canon Printers, scanners and ink</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/81-BTEs98gL._SL1500_.jpg" width="300" height="200" alt="" />
												<h2>$550.00</h2>
												<p>Dell C1760nw LED</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/epson-expression-home-nx-400-printer.jpg" width="300" height="200" alt="" />
												<h2>$556.00</h2>
												<p>Epson Expression Home nx-400</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/aaaaaaa.jpg" width="300" height="200" alt="" />
												<h2>$450.00</h2>
												<p>Live Tech LT 10 mega pixel</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/kensington_.jpg" width="300" height="180" alt="" />
												<h2>$430.00</h2>
												<p>Kensington Orbit Wireless Mobile Trackball</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/513bajXLb8L._SY355_.jpg" width="300" height="180" alt="" />
												<h2>$375.00</h2>
												<p>Microsoft Sculpt Ergonomic Mouse (L6V-00001),Black</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="tablets" >
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/tablet-samsung-tela.jpg" width="300" height="200" alt="" />
												<h2>$780.00</h2>
												<p>Samsung Galaxy Tab 10.1</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/apple_ipad_2.jpg" width="300" height="200" alt="" />
												<h2>$1,400.00</h2>
												<p>Ipad 3 De 16gb Para 3g Mod A1403</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/ASUS_MeMO_Pad8_LTE_News.jpg" width="300" height="200" alt="" />
												<h2>$900.00</h2>
												<p>ASUS Memo Pad 8 (ME581CL)</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="software" >
								<div class="col-sm-12">
									<h2>Software is Empty</h2>
									<img src="public/img/404/404.png" width="300" height="200" alt="" />
								</div>
							</div>
							<div class="tab-pane fade active in" id="phones" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/lenovo.jpg" width="150" height="200" alt="" />
												<h2>$570.00</h2>
												<p>Lenovo PHAB Plus</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/sony-xperia.jpg" width="150" height="200"alt="" />
												<h2>$270.00</h2>
												<p>Sony Xperia Z1 (Honami)</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/lgphone.jpg" width="150" height="200" alt="" />
												<h2>$590.00</h2>
												<p>LG G4</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="public/img/depan/mi4i.jpg" width="150" height="200" alt="" />
												<h2>$480.00</h2>
												<p>Xiaomi Mi 4i</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<br>
					<br>
					<br>
					<div class="recommended_items">
						<h2 class="title text-center">recommended items</h2>
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<div class="item active">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="public/img/depan/iPhone-7-Release.jpg" width="50" height="150" alt="" />
													<h2>$3,000.00</h2>
													<p>iPhone Apple 7</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="public/img/depan/G46_05-copy1.jpg" width="50" height="150" alt="" />
													<h2>$2,000.00</h2>
													<p>ASUS Gaming G46VW</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="public/img/depan/samsung glaxy gearsss.png" width="50" height="130" alt="" />
													<h2>$1,000.00</h2>
													<p>Samsung Glaxy Gear With Flexyble Display Screen</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="item">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="public/img/depan/HV-800-Wireless-.jpg" width="50" height="150" alt="" />
													<h2>$170.00</h2>
													<p>HV-800 Wireless Stereo Bluetooth Headphone</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="public/img/depan/apple-earphones.jpg" width="50" height="170" alt="" />
													<h2>$100.00</h2>
													<p>Apple EarPods Earphones for iPhone</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="public/img/depan/epson.jpg" width="50" height="170" alt="" />
													<h2>$565.00</h2>
													<p>Epson Perfection V600 Scanner</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>			
						</div>
					</div>
				</div>
			</div>
		</div>
	
