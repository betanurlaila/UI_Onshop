<section>
    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="left-sidebar">
            <h2>Category</h2>
            <div class="panel-group category-products" id="accordian"><!--category-productsr-->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                      <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                      Desktops
                    </a>
                  </h4>
                </div>
                <div id="sportswear" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul>
                      <li><a href="#">PC </a></li>
                      <li><a href="#">Mac </a></li>
                      <li><a href="#">See All Desktops </a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordian" href="#mens">
                      <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                      Laptops & Notebooks
                    </a>
                  </h4>
                </div>
                <div id="mens" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul>
                      <li><a href="#">Macs</a></li>
                      <li><a href="#">Windows</a></li>
                      <li><a href="#">See All Laptops & Notebooks</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordian" href="#womens">
                      <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                      Components
                    </a>
                  </h4>
                </div>
                <div id="womens" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul>
                      <li><a href="#">Mice and Trackballs</a></li>
                      <li><a href="#">Monitors</a></li>
                      <li><a href="#">Printers</a></li>
                      <li><a href="#">Scanners</a></li>
                      <li><a href="#">Web Cameras</a></li>
                      <li><a href="#">See All Components</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"><a href="#">Tablets</a></h4>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"><a href="#">Software</a></h4>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"><a href="#">Phones & PDAs</a></h4>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"><a href="#">Cameras</a></h4>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordian" href="#mp3">
                      <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                      MP3 Players
                    </a>
                  </h4>
                </div>
                <div id="mp3" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul>
                      <li><a href="#">Test 3</a></li>
                      <li><a href="#">Test 2</a></li>
                      <li><a href="#">Test 1</a></li>
                      <li><a href="#">See All MP3 Players</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div><!--/category-products-->
          
            <div class="brands_products"><!--brands_products-->
              <h2>Brands</h2>
              <div class="brands-name">
                <ul class="nav nav-pills nav-stacked">
                  <li><a href="#"> <span class="pull-right">(50)</span>iPhone</a></li>
                  <li><a href="#"> <span class="pull-right">(56)</span>Apple</a></li>
                  <li><a href="#"> <span class="pull-right">(27)</span>Canon</a></li>
                  <li><a href="#"> <span class="pull-right">(32)</span>Nikon</a></li>
                  <li><a href="#"> <span class="pull-right">(5)</span>MacBook</a></li>
                  <li><a href="#"> <span class="pull-right">(9)</span>Samsung</a></li>
                  <li><a href="#"> <span class="pull-right">(4)</span>HTC</a></li>
                </ul>
              </div>
            </div><!--/brands_products-->
            
            <div class="price-range"><!--price-range-->
              <h2>Price Range</h2>
              <div class="well text-center">
                 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
                 <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
              </div>
            </div><!--/price-range-->
            
            <div class="shipping text-center"><!--shipping-->
              <img src="public/img/depan/" alt="" />
            </div><!--/shipping-->
          
          </div>
        </div>
        <?php $this->load->view('layouts/partials/menu') ?>
